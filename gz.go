package gz

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"io/ioutil"
)

// CompressString compresses a string.
func CompressString(input string) (string, error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	if _, err := gz.Write([]byte(input)); err != nil {
		return "", err
	}
	if err := gz.Flush(); err != nil {
		return "", err
	}
	if err := gz.Close(); err != nil {
		return "", err
	}
	str := base64.StdEncoding.EncodeToString(b.Bytes())

	return str, nil
}

// DecompressString decompresses a string.
func DecompressString(input string) string {
	data, _ := base64.StdEncoding.DecodeString(input)
	rdata := bytes.NewReader(data)
	r, _ := gzip.NewReader(rdata)
	s, _ := ioutil.ReadAll(r)

	return string(s)
}
